if &compatible
  set nocompatible
endif

" Load packager only when you need it
" Vim:
" git clone https://github.com/kristijanhusak/vim-packager ~/.vim/pack/packager/opt/vim-packager
" Neovim:
" git clone https://github.com/kristijanhusak/vim-packager ~/.config/nvim/pack/packager/opt/vim-packager
function! PackagerInit() abort
  packadd vim-packager
  call packager#init()
  call packager#add('kristijanhusak/vim-packager', { 'type': 'opt' })
  call packager#add('tpope/vim-surround')
  call packager#add('tpope/vim-sensible')
  call packager#add('editorconfig/editorconfig-vim')
  call packager#add('sheerun/vim-polyglot')
  call packager#add('jiangmiao/auto-pairs')

  call packager#add('tpope/vim-eunuch')
  call packager#add('tpope/vim-abolish')
  call packager#add('tpope/vim-repeat')
  call packager#add('tpope/vim-commentary')
  call packager#add('junegunn/fzf.vim')
  call packager#add('Chiel92/vim-autoformat')
endfunction

command! PackagerInstall call PackagerInit() | call packager#install()
command! -bang PackagerUpdate call PackagerInit() | call packager#update({ 'force_hooks': '<bang>' })
command! PackagerClean call PackagerInit() | call packager#clean()
command! PackagerStatus call PackagerInit() | call packager#status()

set number
syntax on
set autoindent

set nobackup
set nowb
set noswapfile

if has('nvim')
  " This should be default
  tnoremap <Esc> <C-\><C-n>
endif

" #################
" ### Commenter ###
" #################
nmap <C-_> gcc

" ##################
" ### AutoFormat ###
" ##################
let g:autoformat_autoindent = 0
let g:autoformat_retab = 0
let g:autoformat_remove_trailing_spaces = 1

au BufWrite * :Autoformat
