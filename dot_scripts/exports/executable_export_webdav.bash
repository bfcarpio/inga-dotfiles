#!/bin/bash
set -eu

printf "Exporting webdav.."
src_path="/home/bfcarpio/inga-compose/webdav"
archive_name="$(date -dlast-monday +%Y%m%d)-webdav.tar.xz"
archive_path="$EXPORT_DIR/$archive_name"

test -f "$archive_path" || { \
	ls -1 ${src_path}/*.xml -t --time=birth -r \
		| awk '{print $0, index($0, "sms")}' \
		| uniq -f 1 \
		| cut -d " " -f 1 \
		| tar -I 'xz -9 -T0' -cvf "$archive_path" -C "$src_path" --files-from=- \
; }

echo "Done"

