#!/bin/bash
set -eu

printf "Exporting sonarr.."
base="/home/bfcarpio/inga-compose/sonarr/config/Backups/scheduled"

name=$(\ls -t "$base" | head -1)

path="$base/$name"

archive_name="$(date -dlast-monday +%Y%m%d)-sonarr.xz"
archive_path="$EXPORT_DIR/$archive_name"

test -f "$archive_path" || { unzip -p "$path" | xz -9 -T0 > "$archive_path"; }

echo "Done"
