#!/bin/bash
set -eu

printf "Exporting postgresql.."
pg_dumpall \
				-h localhost \
				-p 5432 \
				-U postgres \
				--exclude-database=*voter* \
				--exclude-database=facebook \
				--exclude-database=clubhouse \
				--exclude-database=spzz \
| xz -9 -T0 > "$EXPORT_DIR/$(date +%Y%m%d)_pg.xz"
echo "Done"
