#!/bin/bash
set -eu

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
echo "Executing *all* export scripts in $SCRIPT_DIR"
find "$SCRIPT_DIR" -name 'export_*.bash' -type f -not -path "$SCRIPT_DIR/export_all.bash" | xargs -n1 bash
