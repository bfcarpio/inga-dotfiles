#!/bin/bash
set -eu

printf "Exporting mysql.."
mysqldump \
	--host localhost \
	--port 3306 \
	--protocol TCP \
	--all-databases \
	--no-tablespaces \
| xz -9 -T0 > "$EXPORT_DIR/$(date +%Y%m%d)_mysql.xz"
echo "Done"
